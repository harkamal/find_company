class Organization < ActiveRecord::Base
  validates :org_number, presence: true, uniqueness: true
  has_and_belongs_to_many :keywords, :uniq => true
end
