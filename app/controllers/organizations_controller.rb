class OrganizationsController < ApplicationController
	respond_to :json, :xml, :html
  # GET /organizations
  # GET /organizations.json
  # GET /organizations.xml
  def index
    if params[:what].present?
      @search_query = params[:what].strip
      @organizations = OrganizationsSearch.search(@search_query)
      flash[:notice] = "No results." if @organizations.blank?
    else
      @organizations = Organization.none
    end
    respond_with @organizations
  end
end
