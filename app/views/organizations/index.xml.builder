xml.instruct!
xml.organizations do
  @organizations.each do |organization|
    xml.organization do
      xml.name organization.name
      xml.org_number organization.org_number
      xml.city organization.city
    end
  end
end
