json.array!(@organizations) do |organization|
  json.extract! organization, :name
  json.extract! organization, :org_number
  json.extract! organization, :city
end
