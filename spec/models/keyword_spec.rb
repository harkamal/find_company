require 'rails_helper'

RSpec.describe Keyword, type: :model do

  # Verify validation using shoulda matchers
  describe 'validation' do
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
  end

  # The `have_and_belong_to_many` matcher is used to test that a
  # `has_and_belongs_to_many` association exists on your model and that the
  # join table exists in the database.
  describe 'association' do
    it { should have_and_belong_to_many(:organizations) }
  end
end
