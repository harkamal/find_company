require 'rails_helper'

RSpec.describe Organization, type: :model do
    
  # Verify validation using shoulda matchers
  describe Organization, 'validation' do
    it { should validate_presence_of(:org_number) }
    it { should validate_uniqueness_of(:org_number) }
  end

   # The `have_and_belong_to_many` matcher is used to test that a
  # `has_and_belongs_to_many` association exists on your model and that the
  # join table exists in the database.
  describe Organization, 'association' do
    it { should have_and_belong_to_many(:keywords) }
  end
end
