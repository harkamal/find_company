require 'rails_helper'

RSpec.describe "organizations/index", type: :view do
  before(:each) do
    assign(:organizations, [
      Organization.create!(
        :org_number => "1234567"
      )
    ])
  end

  it "renders a list of organizations" do
    render
    assert_select "tr>td", :text => "1234567".to_s, :count => 1
  end
end
