require 'rails_helper'

RSpec.describe "OrganizationsSearch" do

  let(:keyword) { create(:keyword) }
  let(:expiry_time) { Time.now - DEFAULT_CACHE_DURATION }
  let(:expired_keyword) {
    keyword.update_attributes(created_at: expiry_time, updated_at: expiry_time)
    keyword
  }
  let(:organization) { create(:organization) }

  describe "cache not expired" do
    it "returns true for the keyword recently added" do
      expect(OrganizationsSearch.cache_not_expired?(keyword)).to be true
    end

    it "returns false for the keyword added before expiry time" do
      expect(OrganizationsSearch.cache_not_expired?(expired_keyword)).to be false
    end
  end

  describe "search" do
    it "return results from cache" do
      keyword.organizations << organization
      expect(OrganizationsSearch).to_not receive(:find_companies_on_allabolag)
      search_results = OrganizationsSearch.search(keyword.name)
      expect(search_results).to eq(keyword.organizations)
    end

    it "request allabolag and verify html parsed results" do
      search_query = expired_keyword.name
      search_results = OrganizationsSearch.search(search_query)
      expect(search_results.count).to eq(4)
      expect((search_results.first.is_a? Organization)).to be true
    end
  end

end
