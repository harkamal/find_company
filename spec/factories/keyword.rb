FactoryGirl.define do
  factory :keyword do
    sequence(:name){|n| "blocket #{n}" }
  end
end
