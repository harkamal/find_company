FactoryGirl.define do
  factory :organization do
    sequence(:name){|n| "google #{n}" }
    city 'stockholm'
    sequence(:org_number) { |n| "8897478-#{n}" }
  end
end
