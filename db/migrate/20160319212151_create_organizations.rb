class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string :org_number

      t.timestamps null: false
    end
  end
end
