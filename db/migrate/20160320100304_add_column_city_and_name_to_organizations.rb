class AddColumnCityAndNameToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :city, :string
    add_column :organizations, :name, :string
  end
end
