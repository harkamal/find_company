class CreateKeywordsOrganizations < ActiveRecord::Migration
  def change
    create_table :keywords_organizations, id: false do |t|
      t.belongs_to :organization, index: true
      t.belongs_to :keyword, index: true
    end
  end
end
