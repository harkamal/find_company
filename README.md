
## This is a sample application which provides the company org.nr.

## Application have one view where you can enter the organization name and get the org.nr. In particular, as a first debugging check I suggest getting the test suite to pass on your local machine:

```no-highlight
git clone https://gitlab.com/harkamal/find_company.git
cd find_company/
bundle install
# update mysql DB credentials
cp config/database.yml.example config/database.yml
bundle exec rake db:create db:migrate
bundle exec rake db:test:prepare
bundle exec rspec spec/
bundle exec rails s
```

# Application also have a ruby-program which acts as a client and respond with our org.nr. can be run with:
```no-highlight
ruby ./client.rb apoex ab
```
