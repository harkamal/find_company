#!/usr/bin/env ruby
require File.expand_path('../config/environment', __FILE__)
require_relative 'lib/organizations_search'
require 'table_print'
class Client < Struct.new(:argv)
  attr_reader :search_query
  
  def run
    @search_query = self.argv.join("+")
    if search_query.blank?
      puts "Please provide the name of the company you are looking for."
      puts "ex: ruby ./client.rb blocket"
      return
    else      
      find_companies
    end
  end

  def find_companies
    puts "Searching companies for Keyword: '#{search_query}'"
    search_results = OrganizationsSearch.search search_query
    if search_results.present?
      display_search_results(search_results)
    else
      puts "No result found for '#{search_query}'"
    end
  end

  def display_search_results(search_results)
    tp search_results, :org_number, :name, :city
  end
end
c = Client.new(ARGV).run