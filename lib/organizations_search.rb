module OrganizationsSearch
  # OpenURI is an easy-to-use wrapper for Net::HTTP, Net::HTTPS and Net::FTP.
  require 'open-uri'

  # Lookup DB if cache not expired for the search query or
  # request allabolag and get the search results
  # @param search_query [String]
  # @return [Organizations]
  def self.search search_query
    keyword = Keyword.find_or_create_by(name: search_query)
    if keyword.organizations.present? && cache_not_expired?(keyword)
      Rails.logger.info "Return from cache...."
      @organizations = keyword.organizations
    else
      keyword.touch
      @organizations = find_companies_on_allabolag(search_query, keyword)
    end
  end

  # request allabolag and get the search results
  # @param search_query [String]
  # @param keyword [Keyword]
  # @return [Organizations]
  def self.find_companies_on_allabolag(search_query, keyword)
    url = "#{ALLABOLAG_SITE_URL}?#{ALLABOLAG_QUERY_KEY}=#{URI.escape(search_query)}"
    begin
      html_search_result = Nokogiri::HTML(open(url))
      seq = 0
      html_search_result.css('.hitlistLink').each do |company_row|
        content = html_search_result.css('.text11grey6')[seq].content
        city = html_search_result.css('.hitlistCityMap')[seq].text.gsub("Visa på karta", "")
        name = company_row.text
        org_number = content.match(/Org\.nummer: (\d+-\d+|\w+\s\w+)/)[1] rescue content.match(/Org\.nummer: (\d+-XXXX)/)[1]
        organization = Organization.find_or_create_by(org_number: org_number, name: name, city: city) 
        keyword.organizations << organization unless keyword.organizations.include?(organization)
        seq +=1
      end
      keyword.organizations
    rescue Exception => e
      Rails.logger.info 'Something went wrong in parsing....'
      Rails.logger.info e.message
      keyword.organizations
    end
  end

  # Compare last upadted keyword with the default expiry time 
  # @param keyword [Keyword]
  # @return true/false [Boolean]
  def self.cache_not_expired? keyword
    (keyword.updated_at > (Time.now - DEFAULT_CACHE_DURATION))
  end
end
